export const environment = {
  production: false,
  api: {
    url: 'http://localhost:666/dev'
  },
  mapbox: {
    accessToken: 'TOKEN',
    style: 'STYLE'
  }
};
