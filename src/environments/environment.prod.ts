export const environment = {
  production: true,
  api: {
    url: 'https://api.mpr.guilhemallaman.net/api'
  },
  mapbox: {
    accessToken: 'pk.eyJ1IjoiZ3VpbGhlbWFsbGFtYW4iLCJhIjoiY2tvcjA0emI1MGZrNjJ2cDYxNG11Z2owMiJ9.ultR129kIoENUGvZbiJreQ',
    style: 'https://guilhem-street-map.guilhemallaman.net/styles/klokantech-basic/style.json'
  }
};
