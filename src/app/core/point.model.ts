export class Point {

    constructor(
        public x: number,
        public y: number
        ) {
    }

    public asArray(): Array<number> {
        return [this.x, this.y];
    }
}
